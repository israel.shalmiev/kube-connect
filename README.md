# Kube-Connect
## Introduction
This document describes the kube-connect tool, this tool helps to control helm chart and k8s components from remote machine.

## Prerequisites
* prepare machine with last docker, k8s and helm version
* Environment IPs (IPv4 or IPv6)
* **SSH connection to the environment - this tool is using SSH connection once in installation!**

## Who is this tool for?
k8s cluster administrators and QA teams.

## What can I do with it?
Connect to remote k8s cluster and fully control it **without access to OS components** of remote machine

## How to run
You can run this tool by the command below:

```bash
$ ./kube-connect -6 [fd00:10:110:91:240:56ff:fead:4a6a] -u root
```


## Usage
```bash
usage: ./kube-connect -4|--ipv4 <ipv4> or -6|--ipv6 <[ipv6]> -u|--username <master's-user-name> 
 -4|--ipv4 master's ip v4
 -6|--ipv6 master's ip v6 (please use brackets )
 * if both ipv4 and ipv6 specified, ipv4 selected
 -u|--username master's user name
* supports bash or zsh
```


## What is included in installation package?
- kubectx and kubens installed after connection, for more info please refer: https://github.com/ahmetb/kubectx
- aliases created:
    1. for docker use -  d
    2. for kubectl use - k
    3. for helm use - h


# Examples
![kube-connect](/uploads/c078dc3404693b5ac89b3de6770e07a5/kube-connect.gif)